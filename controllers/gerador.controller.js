const estadoCivilModel = require("../models/estado-civil.model");
const optionsMapper = require("../utils/mappers/select-options.mapper");

const fs = require("fs");
const Joi = require("joi");
const ejs = require("ejs");
const htmlToPdf = require("html-pdf-node");
const path = require("path");
const moment = require("moment");

const getGerador = (req, res, next) => {

  const viewModel = {
    nome: "",
    opcoesEstadoCivil: optionsMapper("descricao","id",estadoCivilModel.listaTodos()),
  };

  // construir a view
  res.render("gerador", viewModel);
};

const postGerador = (req, res, next) => {

  // montar o viewmodel
  const { nome, lgnd, email, datanascimento, telefone, estadocivil, imagemUrl } = req.body;
  const estadoCivilSelecionado = estadoCivilModel.BuscaPorId(estadocivil);
  const datanascimentoformatado = moment(datanascimento).format("DD/MM/YYYY");

  const logoUrlImagen = "https://static.casadedios.org/2017/08/legendarios.jpg"


  const pdfViewModel = {
    nome,
    lgnd, 
    datanascimento:datanascimentoformatado,
    email,
    telefone,
    estadocivil: estadoCivilSelecionado.descricao,
    imagemUrl,
    logoUrlImagen
  };
    
  // montar o html
  const filePath = path.join(__dirname, "../views/gerador-pdf.ejs");

  console.log(filePath);
  const templateHtml = fs.readFileSync(filePath, 'utf8');
  
  // montar o pdf
  const htmlPronto = ejs.render(templateHtml, pdfViewModel);

  // retornar o pdf
  const file = {
    content: htmlPronto  
  };

  const configuracoes = {
    format: 'A4',
    printBackground: true
  };

  htmlToPdf.generatePdf(file, configuracoes)
  .then((resultPromessa) => {
      res.contentType("application/pdf");
      res.send(resultPromessa);
  });

};

const postGeradorSchema = Joi.object({
  nome: Joi.string().max(50).min(5).required().custom((value, helpers) => {
    const result = value.split(' ');
    if (result.length > 1) {
      return value;
    }
    return helpers.error("Dado Inválido");
  }),
  lgnd: Joi.string().required(),
  email:  Joi.string().email().required(),
  datanascimento: Joi.date().iso().required(),
  telefone: Joi.string().required(),
  estadocivil: Joi.string().required(),
  imagemUrl: Joi.string().allow(""),
  logoUrlImagen: Joi.string(),
// }).unknown(true);
});

const getGeradorSchema = Joi.object({
  teste: Joi.number().required(),
});

module.exports = {
  getGerador,
  postGerador,
  postGeradorSchema,
  getGeradorSchema
};
