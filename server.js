const express = require("express");
const servidor = express();
const path = require("path");
const bp = require("body-parser");

const rotas = require("./routes/rotas");

// const PORT = process.env.PORT || 3001



servidor.use(bp.json());
servidor.use(bp.urlencoded());

rotas(servidor);
// instalacao e configuracao do EJS
servidor.use(express.static(path.join(__dirname, "public"))); // apontar pasta publica para rodar os assets ("estoque")
servidor.set('views', path.join(__dirname, "views")) // informar o express onde está a pasta view
servidor.set("view engine", "ejs"); // escolher o template enginner para rodar o "html" do projeto




servidor.listen(process.env.PORT || 5000, () => {

});

