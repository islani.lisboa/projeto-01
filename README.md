
# Carteirinha Digital


Application developed to generate a digital card. Can be used for clubs


# Run Locally

Clone the project

```bash
  git clone https://gitlab.com/islani.lisboa/projeto-01.git
```

Go to the project directory

```bash
  cd projeto-01
```

Install dependencies

```bash
  npm install
```

Start the server

```bash
  npm run start
```

## Deployment

https://projeto-01.herokuapp.com



## Used By

This project is used by the following company:

- Legendarios


  
## Authors

- [@Islani](https://www.linkedin.com/in/islanilisboa/)

  