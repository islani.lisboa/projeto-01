const geradorController = require("../../controllers/gerador.controller");
const middlewareValidaDTO = require("../../utils/dto-validate");

module.exports = (routeV1) => {
  routeV1
    .route("/gerador")
    .get(geradorController.getGerador)
    .post(middlewareValidaDTO("body", geradorController.postGeradorSchema), geradorController.postGerador);
};
