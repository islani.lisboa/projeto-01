
const express = require("express");
const v1Home = require('./v1/home');
const v1Gerador = require('./v1/gerador');


module.exports = (app) => {
    const routev1 = express.Router();
    
    v1Home(routev1);
    v1Gerador(routev1);

    app.use('', routev1);
}

