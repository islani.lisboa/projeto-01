
const estadoCivilDB = [
    {id: 1, descricao: "Solteiro",},
    { id: 2, descricao: "Casado",},
    { id: 3, descricao: "Divorciado",},
    { id: 4, descricao: "Viuvo",},
];

const listaTodos = () => {
    return estadoCivilDB;
}

const BuscaPorId = (id) => {
    const result = estadoCivilDB.filter((item) => {
        return item.id === Number(id);
    });
    return result.length > 0 ? result[0] : undefined;
}

const criaNovoCivil = ({ id, descricao }) => {
    estadoCivilDB.push({ id, descricao });
}

module.exports = {
    listaTodos,
    BuscaPorId,
    criaNovoCivil
}